---
title: "Frank J. Robinson"
date: 2018-02-05
age: 17
emergency_contact: +1 (555) 555 5555
year: junior
location: 43.3388,-1.781
---

Frank, our number one second baseman!

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam quam, tristique elementum blandit sed, ullamcorper ut erat. Suspendisse mattis aliquet urna in aliquet. Integer venenatis ligula in justo venenatis, eleifend luctus mi commodo. Quisque elementum odio et ipsum imperdiet vestibulum. Etiam libero nunc, egestas quis erat ut, hendrerit tristique nisl. Aliquam malesuada nunc at dignissim efficitur. Proin ex ante, efficitur id ante sed, condimentum egestas orci. Integer in ornare metus.

Mauris fringilla libero non tellus interdum, ut pharetra nisi porttitor. Nunc ac est dictum est mattis aliquet sit amet sit amet tortor. Nulla scelerisque mollis rutrum. Donec non accumsan leo, non placerat diam. Donec scelerisque vehicula mi, ac ultrices lectus ultrices ac. Sed luctus magna eget massa lacinia, et consectetur lacus dapibus. Proin dapibus nunc vulputate viverra posuere. In at varius justo, at porttitor urna. Donec sagittis tempor fermentum. Proin consectetur cursus euismod. Nulla sodales non justo ac venenatis. Morbi euismod elit vulputate sapien aliquet elementum. Suspendisse sit amet nibh elit. Phasellus bibendum lorem ac commodo facilisis.

Ut ante mi, auctor at enim vel, pellentesque cursus nunc. Proin cursus ex eu neque suscipit, et mattis libero facilisis. Cras nunc purus, scelerisque fermentum ante ac, pellentesque viverra quam. Nam elementum tortor at ultrices blandit. Phasellus quis massa scelerisque, imperdiet risus in, eleifend nisl. Praesent sit amet lorem pulvinar dolor scelerisque tincidunt eget ac mauris. Curabitur nibh nisi, laoreet sed lorem at, ultrices molestie velit. Vivamus tristique mi sit amet varius fermentum. Donec bibendum tortor at sollicitudin finibus.

Pellentesque vehicula augue quis sapien ultrices, quis commodo mi dignissim. Nulla scelerisque odio turpis, et pulvinar sem consectetur non. Morbi dapibus aliquet tristique. Ut tristique pretium efficitur. Vivamus risus arcu, varius et efficitur vitae, suscipit at ligula. Nullam in est tincidunt, volutpat purus non, consectetur urna. Pellentesque risus erat, posuere vitae erat at, dignissim porttitor augue. Vivamus vitae est ligula. Donec commodo erat nec ante gravida, nec luctus risus efficitur. Suspendisse tempor sagittis dui sed ullamcorper. Etiam a ultricies urna, sagittis viverra justo. Nulla porta tincidunt lorem, ut porttitor dolor malesuada eget. Maecenas egestas, ligula sed cursus iaculis, nibh neque tempor massa, sed bibendum lectus lorem vitae erat. Nam dignissim nunc lorem, a rhoncus justo aliquet eget.

Nulla rutrum justo id porttitor luctus. Mauris ornare erat nec lacus finibus semper. Cras purus nisi, rutrum egestas lorem ultricies, consectetur condimentum nisi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam varius magna sit amet sagittis volutpat. Aliquam eget sollicitudin justo. In tempor mi ac egestas scelerisque. Aenean a eros a nunc ornare condimentum a quis libero. Nulla suscipit lobortis condimentum. Integer tristique in augue ac bibendum. Quisque molestie, dui in egestas pulvinar, risus risus posuere nulla, a hendrerit turpis risus at lorem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur rutrum convallis lacinia.
